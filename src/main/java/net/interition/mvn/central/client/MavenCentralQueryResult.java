package net.interition.mvn.central.client;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class MavenCentralQueryResult {
	
	private ResponseHeader header;
	private Response response;

}
