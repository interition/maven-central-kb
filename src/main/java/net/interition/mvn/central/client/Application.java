package net.interition.mvn.central.client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class Application implements CommandLineRunner {

	private static final Logger log = LoggerFactory
			.getLogger(Application.class);

	public static void main(String args[]) {
		SpringApplication.run(Application.class);
	}

	// @Override
	public void run(String... strings) throws Exception {
		RestTemplate restTemplate = new RestTemplate();

		String request = "http://search.maven.org/solrsearch/select?q=guice&rows=1&wt=json";

		MavenCentralQueryResult result = restTemplate.getForObject(request,
				MavenCentralQueryResult.class);

		// log.info(result.toString());

		Response response = result.getResponse();

		System.out.println("Number of projects found: "
				+ response.getNumFound());

		request = "http://search.maven.org/solrsearch/select?q=activemq&rows="
				+ response.getNumFound() + "&wt=json";

		System.out.println("New request: "
				+ request);
		
		result = restTemplate.getForObject(request,
				MavenCentralQueryResult.class);
		
		response = result.getResponse();
		
		Doc[] docs = response.getDocs();

		for (Doc d : docs) {

			// System.out.print("Id: " + d.getId() + ", Group Id: " + d.getG()
			// + ", Artifact Id: " + d.getA() + ", Latest Version: "
			// + d.getLatestVersion());

			// these attributes are being pulled from the Text array in the Docs
			// object
			// for (String attribute : d.getText()) {
			// System.out.println(attribute);
			// }

			// assemble resource url prfix
			// http://search.maven.org/remotecontent?filepath=io/prometheus/client/examples/guice/0.0.9/guice-0.0.9.pom
			// "http://search.maven.org/remotecontent?filepath=" +

			StringBuffer url = new StringBuffer(
					"http://search.maven.org/remotecontent?filepath=");

			url.append(d.getG().replace(".", "/") + "/");
			url.append(d.getA().replace(".", "/") + "/");
			url.append(d.getLatestVersion() + "/");
			url.append(d.getA() + "-" + d.getLatestVersion());

			System.out.println(url);

			// for each of the "ec" array contruct a uri
			for (String suffix : d.getEc()) {
				System.out.println(url + suffix);
			}

		}

	}
}