package net.interition.mvn.central.client;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Doc {
	
	private String id;
	private String g;
	private String a;
	private String latestVersion;
	private String repositoryId;
	private String p;
	private String timestamp;
	private String versionCount;
	
	private String[] text;
	
	private String[] ec;

}
