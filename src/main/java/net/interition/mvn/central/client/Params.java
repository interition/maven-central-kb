package net.interition.mvn.central.client;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Params {
	
	private boolean spellcheck;
	private String fl;
	private String sort;
	private String indent;
	private String q;
	private String qf;
	private String version ;


}
