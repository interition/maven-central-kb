package net.interition.mvn.central.client;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Response {
	
	private String numFound;
	private String start;
	private Doc[] docs;

}
