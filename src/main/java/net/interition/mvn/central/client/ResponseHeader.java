package net.interition.mvn.central.client;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ResponseHeader {
	
	private String status;
	private String QTime;
	
	private Params params;

}
